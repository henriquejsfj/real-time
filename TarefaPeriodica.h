//
// Created by henrique on 06/09/2019.
//

#ifndef REAL_TIME_TAREFAPERIODICA_H
#define REAL_TIME_TAREFAPERIODICA_H

#include <iostream>

using namespace std;

class TarefaPeriodica {

public:
    int tempoAtivacao;
    int tempoDeadline;
    int periodo;
    int tempoDuracao;
    int id;
    int resta;

    TarefaPeriodica(int num_id, int per, int tDuracao, int tAtivacao, int tDeadline);
    TarefaPeriodica(int num_id, int per, int tDuracao);
    TarefaPeriodica();
    TarefaPeriodica(const TarefaPeriodica&);
    bool execute();
    void print();
};


#endif //REAL_TIME_TAREFAPERIODICA_H
