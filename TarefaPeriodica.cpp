//
// Created by henrique on 06/09/2019.
//

#include "TarefaPeriodica.h"

TarefaPeriodica::TarefaPeriodica(int num_id, int per, int tDuracao, int tAtivacao, int tDeadline) {
    tempoAtivacao = tAtivacao;
    tempoDeadline = tAtivacao + tDeadline;
    tempoDuracao = tDuracao;
    resta = tempoDuracao;
    periodo = per;

    id = num_id;
}

TarefaPeriodica::TarefaPeriodica(int num_id, int per, int tDuracao) {
    tempoAtivacao = 0;
    tempoDeadline = per;
    tempoDuracao = tDuracao;
    resta = tempoDuracao;
    periodo = per;

    id = num_id;
}

TarefaPeriodica::TarefaPeriodica() {

}

TarefaPeriodica::TarefaPeriodica(const TarefaPeriodica & old) {
    tempoDuracao = old.tempoDuracao;
    tempoDeadline = old.tempoDeadline;
    tempoAtivacao = old.tempoAtivacao;
    resta = old.resta;
    periodo = old.periodo;
    id = old.id;
}

bool TarefaPeriodica::execute() {
    resta -= 1;
    if (resta == 0){
        resta = tempoDuracao;
        tempoAtivacao += periodo;
        return true;
    }
    return false;
}

void TarefaPeriodica::print() {
    cout << id << " " << periodo << " " << tempoDuracao << " " << tempoAtivacao << " " << tempoDeadline << endl;
}