//
// Created by henrique on 06/09/2019.
//

#include "Solver.h"

template <typename Number>
Number gcd(Number u, Number v) {
    while (v != 0) {
        Number r = u % v;
        u = v;
        v = r;
    }
    return u;
}

template <typename Number>
Number lcm(Number u, Number v) {
    return (u*v)/gcd(u, v);
}

Solver::Solver(TarefaPeriodica *tasks, int num, int *tempos, int t_simu) {

    lista_tarefa = tasks;
    vetor_tempos = tempos;
    num_tarefas = num;
    tempo_simu = t_simu;
}

void Solver::print() {
    cout << "Imprimindo as tarefas: " << endl;
    for (int i=0; i<num_tarefas; ++i){
        lista_tarefa[i].print();
    }
    cout << "Imprimindo o resultado: " << endl;
    for (int i=0; i<tempo_simu; ++i){
        cout << vetor_tempos[i] << " ";
    }
    cout << endl;
}

int * Solver::edf() {
    int deadline, id_min, min_deadline;
    TarefaPeriodica copy_lista_tarefa[10];
    for (int i = 0; i < num_tarefas; ++i){
        TarefaPeriodica temp = lista_tarefa[i];
        copy_lista_tarefa[i] = temp;
    }
    vector<int> tarefas_ativas;
    // For each time step the solver has to decide what task to execute
    for (int t=0; t<tempo_simu; ++t){
        for (int task=0; task<num_tarefas; ++task){
            // If the task was activated now:
            if (copy_lista_tarefa[task].tempoAtivacao == t){
                // Insert it at the activated tasks list
                tarefas_ativas.push_back(copy_lista_tarefa[task].id);
            }
        }
        min_deadline = tempo_simu*2;
        id_min = -1;
        // Now the solver has to look the activated tasks
        for (auto it = tarefas_ativas.begin(); it != tarefas_ativas.end(); ++it){
            deadline = copy_lista_tarefa[*it].tempoDeadline + copy_lista_tarefa[*it].tempoAtivacao;
            // Find the task with Earliest Deadline
            if (deadline < min_deadline){
                min_deadline = deadline;
                id_min = *it;
            }
        }
        // At this time execute the task with Earliest Deadline
        if (copy_lista_tarefa[id_min].execute() && id_min!=-1) { // If we finished the task
            // Remove it from activated tasks
            tarefas_ativas.erase(remove(tarefas_ativas.begin(), tarefas_ativas.end(), id_min),
                                 tarefas_ativas.end());
        }
        vetor_tempos[t] = id_min; // Register this execution
    }
    return vetor_tempos;
}

int * Solver::dm() {
    vector<int> tarefas_ativas;
    int deadline, id_min, min_deadline;
    TarefaPeriodica copy_lista_tarefa[10];
    for (int i = 0; i < num_tarefas; ++i){
        TarefaPeriodica temp = lista_tarefa[i];
        copy_lista_tarefa[i] = temp;
    }
    for (int t=0; t<tempo_simu; ++t){ // For each time step the solver has to decide what task to execute
        for (int task=0; task<num_tarefas; ++task){
            if (copy_lista_tarefa[task].tempoAtivacao == t){ // If the task was activated now:
                tarefas_ativas.push_back(copy_lista_tarefa[task].id); // Insert it at the activated tasks list
            }
        }
        min_deadline = tempo_simu*2;
        id_min = -1;
        for (auto it = tarefas_ativas.begin(); it != tarefas_ativas.end(); ++it){ // Now the solver has to look the activated tasks
            deadline = copy_lista_tarefa[*it].tempoDeadline;
            if (deadline < min_deadline){ // Find the task with Earliest Deadline
                min_deadline = deadline;
                id_min = *it;
            }
        }
        // At this time execute the task with Earliest Deadline
        if (copy_lista_tarefa[id_min].execute() && id_min!=-1) { // If we finished the task
            // Remove it from activated tasks
            tarefas_ativas.erase(remove(tarefas_ativas.begin(), tarefas_ativas.end(), id_min),
                                 tarefas_ativas.end());
        }
        vetor_tempos[t] = id_min; // Register this execution
    }
    return vetor_tempos;
}

int * Solver::rm() {
    vector<int> tarefas_ativas;
    int min_periodo, id_min, periodo;
    TarefaPeriodica copy_lista_tarefa[10];
    for (int i = 0; i < num_tarefas; ++i){
        TarefaPeriodica temp = lista_tarefa[i];
        copy_lista_tarefa[i] = temp;
    }
    for (int t=0; t<tempo_simu; ++t){ // For each time step the solver has to decide what task to execute
        for (int task=0; task<num_tarefas; ++task){
            if (copy_lista_tarefa[task].tempoAtivacao == t){ // If the task was activated now:
                tarefas_ativas.push_back(copy_lista_tarefa[task].id); // Insert it at the activated tasks list
            }
        }
        min_periodo = tempo_simu*2;
        id_min = -1;
        for (auto it = tarefas_ativas.begin(); it != tarefas_ativas.end(); ++it){ // Now the solver has to look the activated tasks
            periodo = copy_lista_tarefa[*it].periodo;
            if (periodo < min_periodo){ // Find the task with Earliest Deadline
                min_periodo = periodo;
                id_min = *it;
            }
        }
        // At this time execute the task with Earliest Deadline
        if (copy_lista_tarefa[id_min].execute() && id_min!=-1) { // If we finished the task
            // Remove it from activated tasks
            tarefas_ativas.erase(remove(tarefas_ativas.begin(), tarefas_ativas.end(), id_min),
                                 tarefas_ativas.end());
        }
        vetor_tempos[t] = id_min; // Register this execution
    }
    return vetor_tempos;
}

int * Solver::ce() {
    vector<int> tarefas_ativas;
    int id_min, min_periodo, periodo, mmc, mdc;
    bool executando = false;
    TarefaPeriodica copy_lista_tarefa[10];
    for (int i = 0; i < num_tarefas; ++i){
        TarefaPeriodica temp = lista_tarefa[i];
        copy_lista_tarefa[i] = temp;
    }

    mmc = copy_lista_tarefa[0].periodo;
    mdc = copy_lista_tarefa[0].periodo;
    for (int task = 1; task < num_tarefas; ++task) {
        mdc = gcd(copy_lista_tarefa[task].periodo, mdc); // calculate gcd
        mmc = lcm(copy_lista_tarefa[task].periodo, mmc); // calculate lcm
    }
    for (int t=0; t<tempo_simu; ++t) { // For each time step the solver has to decide what task to execute
        for (int task=0; task<num_tarefas; ++task){
            if (copy_lista_tarefa[task].tempoAtivacao == t){ // If the task was activated now:
                tarefas_ativas.push_back(copy_lista_tarefa[task].id); // Insert it at the activated tasks list
            }
        }
        if (!executando){ // If not executing we should findo something to execute
            min_periodo = tempo_simu*2;
            id_min = -1;
            for (auto it = tarefas_ativas.begin(); it != tarefas_ativas.end(); ++it){ // Now the solver has to look the activated tasks
                periodo = copy_lista_tarefa[*it].periodo;
                if (periodo < min_periodo){ // Find the task with least period
                    min_periodo = periodo;
                    id_min = *it;
                }
            }
            if(copy_lista_tarefa[id_min].tempoDuracao >= mdc-t%mdc){ // See if we can execute this task at this minor cycle
//                t += mdc - t%mdc - 1;
                continue;
            }
            executando = true;
        }

        // At this time execute the task with least period
        if (copy_lista_tarefa[id_min].execute() && id_min!=-1) { // If we finished the task
            // Remove it from activated tasks
            tarefas_ativas.erase(remove(tarefas_ativas.begin(), tarefas_ativas.end(), id_min),
                                 tarefas_ativas.end());
            executando = false; // Free to run other tasks
        }
        vetor_tempos[t] = id_min; // Register this execution

    }
    return vetor_tempos;
}


