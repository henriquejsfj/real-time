# Real-time

This repository was made to the development of codes necessary to the real-time control discipline at the Federal University
 of Rio de Janeiro (UFRJ).

At the classes, we study the concepts of task scheduling and some algorithm to solve it.

So as homework we implemented 5 algorithms to deal with periodic tasks:
- Cyclic Executive
- Rate Monotonic
- Earliest Deadline First
- Deadline Monotonic
- Earliest Deadline First without deadlines restrictions

To solve it we have many restrictions, simplifications:
<!-- TODO: Listar as restricoes usadas -->

## Modeling the problem

Before start coding anything, we first spend time modeling how we would implement it. So, was intuitive to modeled it at
object-oriented paradigm, so our UML diagram ended like this:

![Alt text](tempo_real.png?raw=true "Title")

As we just have a unique type of task that is periodic we created a class called "TarefaPeriodica" (name in Portuguese).
Also, we created a base class to represent the concept of a Solver, we were wondering about representing each scheduling
method as a child class from Solver, but as they all have the same proprieties and just differs by a method, we ended by 
modeling each scheduling method as a method on the Solver base class.

Note that the Solver class receives contains many TarefaPeriodica objects by lista_tarefa attribute, this relation is 
represented at the image.  

## Code

As an object-oriented problem, we choose C++ to deal with it as seems to be natural in task schedule context due to it's
 efficiency.
For each class showed in the previous diagram we have a header file (.h) and the code (.cpp). On the header classes,
there are just declarations of variables and functions. Every definitions are on .cpp codes.

### `TarefaPeriodica`
This class represents the concept of a periodic task. So it should have the attributes that a periodic task have like:
Activation time, deadline time, period and duration time. Also, by implementation design, the task has an id which is the
index at a hash table (which goes on Solver) that contains this object and an attribute to represent the remaining time 
to finish the task. Of course, a task is executable, so it has a method called execute. 

All its attributes are integers as we deal with discrete-time, a list of the attributes and what does it means as follows:
- *`tempoAtivacao`:* Task activation time, at this time the task becomes available to be executed.
- *`tempoDeadline`:* Task deadline time, this is the limit time to end the task execution, it values is relative to task
activation time.
- *`periodo`:* Task period, every this amount of time the task becomes available.
- *`tempoDuracao`:* Task duration time, this is how long it takes to execute the task.
- *`id`:* Task id, is the identifier of the task, it also represents the position at the task array where this task is.
- *`resta`:* Remaining time to end the execution of the task.

There are 3 constructors defined for this class, se the usage bellow:
- `TarefaPeriodica(int num_id, int per, int tDuracao, int tAtivacao, int tDeadline)`: Instantiate a TarefaPeriodica
object associating num_id to `id`, per to `periodo`, tDuracao to `tempoDuracao`, tAtivacao to `tempoAtivacao` and tDeadline to
`tempoDeadline`. The resta value receives the same value as tempoDuracao.
- `TarefaPeriodica(int num_id, int per, int tDuracao)`: Instantiate a TarefaPeriodica object as previous constructor,
but with tempoDeadline equals to periodo and tempoAtivacao being 0. This simplifications are very common.
- `TarefaPeriodica()`: This instantiate an empty TarefaPeriodica object, it should be used only to define arrays.

This class has 2 methods as follows:

- `bool execute()`: Execute the task, this means, decrement it's `resta` value, verify if the task was task was over and
if so reset `resta` to `tempoDuracao` value, add `periodo` to `tempoAtivacao` and return true. If not, return false.
- `void print()`: Print the attributes, except by `resta`, separated by space (" ") with end line. It will print in the
order `id`, `periodo`, `tempoDuracao`, `tempoAtivacao`, `tempoDeadline`. It is for debugging purposes only.

### `Solver`

This class represents the concept of a solver, this means that the solver has to receive a task set to execute and solve
it. Also, this solver has to put the result of scheduling simulation somewhere. To map the task set into the results 
there are many different algorithms implemented as Cyclic Executive, Rate Monotonic, Earliest Deadline First and Deadline
Monotonic. In addition, the solver has to evaluate these schedules, and the methods to do that were declared but not implemented.
 
The attributes are naturally array to handle tasks and simulation result and to manage array in C++ we also have
attributes to store array length. The attributes were defined as follows:

- `lista_tarefas`: Array where its element is a task (`TarefaPeriodica` object) that solver has to schedule. 
TarefaPeriodica should be at the index equal to its id. And that is the way how the task is accessed by solver methods.
- `vetor_tempos`: It contains the result of the last executed schedule method, each element is a time step, starting
time 0 at index 0 and so on.
- `int num_tarefas`: The number of tasks that the solver has to solve and also the length of `lista_tarefa` array.
- `int tempo_simu`: Simulation time, is the end time of schedule solver and also the length of `vetor_tempos` array.

There is just one constructor which receives these 4 attributes as parameters:

- `Solver(TarefaPeriodica *tasks, int num, int *tempos, int t_simu)`: The parameters are associated respectively to 
`lista_tarefas`, `num_tarefas`, `vetor_tempos` and `tempo_simu`.

There are 4 methods that implement schedule algorithms, they all solve the task set with or without deadline restriction. The restriction are in the task having it period equals to deadline and the methods don't verify it, 
 they just solve by it's definition:

- `int * edf()`: Implements the Earliest Deadline First algorithm and returns the `vetor_tempos`.
- `int * rm()`: Implements the Rate Monotonic algorithm and returns the `vetor_tempos`.
- `int * dm()`: Implements the Deadline Monotonic algorithm and returns the `vetor_tempos`.
- `int * ce()`: Implements the Cyclic Executive algorithm and returns the `vetor_tempos`.

Note that the `vetor_tempos` returned is a pointer and if another method is executed the result will be replaced if newest
one.

There are also evaluation methods declared and not implemented, just to future work on this project, and a `print()`
the method which prints the tasks and then the result (of last executed method).