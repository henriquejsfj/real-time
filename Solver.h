//
// Created by henrique on 06/09/2019.
//

#ifndef REAL_TIME_SOLVER_H
#define REAL_TIME_SOLVER_H

#include <vector>
#include <algorithm>

#include "TarefaPeriodica.h"

using namespace std;

class Solver {

protected:
    int num_tarefas;
    TarefaPeriodica *lista_tarefa;
    int *vetor_tempos;
    int tempo_simu;


public:
    Solver(TarefaPeriodica *tasks, int num, int *tempos, int t_simu);
    void print();
    int * edf();
    int * rm();
    int * dm();
    int * ce();
    double tempo_resposta();
    double tempo_medio();
    double tempo_total();
    double soma_ponderada();
    double maximum();
    double atraso_maximo();

};


#endif //REAL_TIME_SOLVER_H
