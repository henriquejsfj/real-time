//
// Created by henrique on 06/09/2019.
//
#include <jsoncpp/json/json.h>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <boost/filesystem.hpp>

#include "TarefaPeriodica.h"
#include "Solver.h"

using namespace std;

struct path_leaf_string
{
    string operator()(const boost::filesystem::directory_entry& entry) const
    {
        return entry.path().leaf().string();
    }
};

void read_directory(const string& name, vector<string>& v)
{
    boost::filesystem::path p(name);
    boost::filesystem::directory_iterator start(p);
    boost::filesystem::directory_iterator end;
    transform(start, end, back_inserter(v), path_leaf_string());
}

template <typename T>
string Str( const T & t ) {
    ostringstream os;
    os << t;
    return os.str();
}

int main(){
    vector<string> v;
    string file;
    read_directory("data/", v);
    int *r;

    for(auto & it : v) {
        file = it.substr(0, it.find('.'));
        ifstream dados_file("data/" + file + ".json", std::ifstream::binary);
        Json::Reader reader;
        Json::Value dados;
        reader.parse(dados_file, dados);

        int x;
        x = dados["config"]["numero_tarefas"].asInt();

        TarefaPeriodica task[x];
        if (dados["config"]["restricao"].asBool()) {
            for (int i = 0; i < x; ++i) {
                task[i] = TarefaPeriodica(i, dados[Str(i)]["periodo"].asInt(), dados[Str(i)]["duracao"].asInt());
            }
        }else{
            for (int i = 0; i < x; ++i) {
                task[i] = TarefaPeriodica(i, dados[Str(i)]["periodo"].asInt(), dados[Str(i)]["duracao"].asInt(), 0,
                        dados[Str(i)]["deadline"].asInt());
            }
        }

        cout << "Successfully populated" << endl;

        int t_simu = dados["config"]["tempo_simu"].asInt();
        int t[t_simu];

        ofstream result_file;
        result_file.open("result/result_" + file + ".csv");
        result_file << "tarefa/tempo";
        for (int i = 1; i <= t_simu; ++i) {
            result_file << "," << i;
        }
        result_file << endl;

        Solver s = Solver(task, x, t, t_simu);

        string met[] = {"EDF: ", "RM: ", "DM: ", "CE: "};
        for (int m  = 0; m < 4; ++m) {
            switch (m){
                case 0:
                    r = s.edf();
                    break;
                case 1:
                    r = s.rm();
                    break;
                case 2:
                    r = s.dm();
                    break;
                case 3:
                    r = s.ce();
                    break;
            }
            s.print();
            for (int ta = 1; ta <= x; ++ta) {
                result_file << met[m] << ta << ",";
                for (int i = 0; i < t_simu; ++i) {
                    if (r[i] == ta - 1) {
                        result_file << "X";
                    }
                    result_file << ",";
                }
                result_file << endl;
            }
        }
        result_file.close();

    }

    return 0;
}
