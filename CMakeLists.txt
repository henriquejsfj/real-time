cmake_minimum_required(VERSION 3.14)
project(real-time)

set(CMAKE_CXX_STANDARD 14)

find_package(Boost 1.55.0 REQUIRED COMPONENTS system filesystem)
find_package(PkgConfig REQUIRED)

include_directories(${Boost_INCLUDE_DIRS})
link_directories(${Boost_LIBRARY_DIRS})

pkg_check_modules(JSONCPP jsoncpp)
link_libraries(${JSONCPP_LIBRARIES})

add_executable(main main.cpp TarefaPeriodica.cpp TarefaPeriodica.h Solver.cpp Solver.h)

target_link_libraries(main ${JSONCPP_LIBRARIES} ${Boost_LIBRARIES})

